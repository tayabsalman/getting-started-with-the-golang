package main

import(
	"fmt"
)

func main(){
	fmt.Println("Hi Everyone")
	fmt.Println(SayGreetings("Greetings..."))
	fmt.Println(SayGoodbye("Good Bye..."))
}

//Below teo function are created as an example
//Every function starts with func keyword, followed by function name
//With parenthesis parameters are mentioned and then return type
func SayGreetings(param string) string{
	return param + " From TayabSalman"
}

func SayGoodbye(param1 string ) string{
	return param1 + " Says Tayabsalman"
}