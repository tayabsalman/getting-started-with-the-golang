package main

import "log"

func main(){
	pet := "Tiger"

	if pet == "cat"{
		log.Println("My pet is a ", pet)
	}else if pet =="dog"{
		log.Println("My pet is a Dog")
	}else if pet == "Hamster"{
		log.Println("My pet is a Hamster")
	}else{
		log.Println("I do not know my pet")
	}
	
}