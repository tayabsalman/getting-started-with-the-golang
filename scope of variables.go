package main

import "log"

//Declaring and initializing of global variable
var color = "GREEN"

func main(){
	//Declaring and initializing of local variable
	color1 := "Blue"
	log.Println("Color1 is : ",color1)  //Accessing local variable
	log.Println("Color is : ",color)  //Trying to access global variable
	color = "PINK"  //Updating a global varialbe's value
	SaySomething(color1)
	SaySomethingElse("GREY")
}

func SaySomething(receivedColor string){
	log.Println("Color receivedd is", receivedColor)
	log.Println("In SaySomethign Color is", color)
}

func SaySomethingElse(color1 string){ 
	//Receiving arguments with different name to avoid Variable shadowing of global color named variable
	log.Println("Color in SomethingElse is : ", color)
}