package main

import "fmt"
func main(){
	//You can declare a variable with var keyword then variablle name then to which type in belongs to
	var myString1 string
	//Then initialize the varaible
	myString1 = "One way of initialization"
	fmt.Println("myString1 is ", myString1)

	//Declaration and initialization can happen at the same time
	var myInt int = 35
	fmt.Println("myInt is, ", myInt)

	//Multiple variables can be declared at the same 
	//amd also be initialized
	var a,b,c,d int = 2022, 2025,2030,2035
	fmt.Println("Values of a,b,c and d are ", a,b, c, d)
}