package main

import "log"
func main(){
	//Declaration of map data structure with the help of make function
	//make is an in-built function
	//Following line creates a map with string keys and its string values
	var myMap = make(map[string]string)

	//This is how a new key with its value can be added

	myMap["name"] = "Tayab"
	myMap["channel"] = "Born To Program"
	myMap["my-other-channel"] = "Something"

	log.Println("HI, My name is :", myMap["name"])
	log.Println("And my channel is :", myMap["channel"])
	log.Println("And my other channel is :", myMap["my-other-channel"])

	//Uncomment below line to see that the order of elements in Map will be same as its initialized
	// log.Println(myMap)

	//A map with string keys and integer values
	myOtherMap :=make(map[string]int)
	myOtherMap["First"] = 1
	myOtherMap["second"] = 2
	log.Println("First : ", myOtherMap["First"])
	log.Println("Second : ", myOtherMap["second"])

	//Declaration of integer slice
	//it can cntain any number of integers in it
	var mySlice []int

	//One way of inserting/appending the values into the slices
	mySlice = append(mySlice, 1)
	mySlice = append(mySlice, 2)
	mySlice = append(mySlice, 3)
	mySlice = append(mySlice, 4)
	mySlice = append(mySlice, 5)

	//we can check that the order of elements in the slice is same as its initialized
	log.Println(mySlice)

	//Slice can be intialized and declared as the same time
	var years = []int{10,20,30,40,50}
	log.Println(len(years))
	log.Println(years)

}