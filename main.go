package main 
//Specifying the package must be the first line of code

//Import required packages/libraries used in the program
import(
	"fmt"
)

//Must have a function named "main"
func main(){
	//fmt package used for printing
	fmt.Println("Let's GO for it")
	fmt.Println("LIke and Subscribe for this channel and Video and Share")
}