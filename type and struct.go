package main

import "log"

//Defining the structure representing User
type User struct{
	FirstName string
	LastName string
	Age int
}

//This function belongs only to the struct User, and can be accessed only by its variables
func (myVar *User)UpdateAge(age int){
	myVar.Age = age
}

func main(){
	//Defining Numbers to be treated as equivalent to int type
	type Numbers int
	var a Numbers
	a=10
	log.Println("a is ", a)

	//Defining text to be treated as equivalent to string type
	type text string
	var name text
	name = "Born To Program"
	log.Println("Name is ", name)

	//Initializing a variable of type User
	user1 := User{
		FirstName:"Tayab",
		LastName:"Salman",  //notice this end comma
	}

	//Accessing the attributes from a structure
	log.Println("First name is ", user1.FirstName)
	log.Println("last name is ", user1.LastName)

	//Passing and receiving the struct variable to and from functions
	user1 = AddAge(user1)
	log.Println("User age is ", user1.Age)
	user1.UpdateAge(30)
	log.Println("User's updated  age is ", user1.Age)
}

//Function takes a param of type User and return param of type User
func AddAge(user User) User{
	user.Age = 20
	return user
}

